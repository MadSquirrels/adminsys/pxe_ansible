
<...> should be replace by the real value

At the begining you should configure the machine as static ip

you need to create 2 file:
group_vars/all/variables.yml
```
---
dhcp:
  username: <username>
  password: <password>
  ip: <ip_address>

tftp:
  username: <username>
  password: <password>
  ip: <ip_address>

```

hosts.yaml
```
all:
  children:
    dhcp:
      hosts: <ip_address>
      vars:
        ansible_user: "{{ dhcp['username'] }}"

```
